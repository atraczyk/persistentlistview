#include "smartlistmodel.h"

#include <QGuiApplication>
#include <QJSEngine>
#include <QQmlApplicationEngine>
#include <QQmlEngine>

#define NS_MODELS "net.smartlist.Models"
#define VER_MAJ 1
#define VER_MIN 0

// clang-format off
#define QML_REGISTERSINGLETONTYPE_POBJECT(NS, I, N) \
    QQmlEngine::setObjectOwnership(I, QQmlEngine::CppOwnership); \
    { using T = std::remove_reference<decltype(*I)>::type; \
    qmlRegisterSingletonType<T>(NS, VER_MAJ, VER_MIN, N, \
                                [I](QQmlEngine*, QJSEngine*) -> QObject* { \
                                    return I; }); }
// clang-format on

int main(int argc, char* argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(
        &engine, &QQmlApplicationEngine::objectCreated,
        &app, [url](QObject* obj, const QUrl& objUrl) {
            if (!obj && url == objUrl)
                QCoreApplication::exit(-1);
        },
        Qt::QueuedConnection);

    ConversationQueue conversations;

    conversations.emplace_back(Conversation { "lol4", 1618000000 });
    conversations.emplace_back(Conversation { "cyrille", 1618000002 });
    conversations.emplace_back(Conversation { "jeff", 1618000004 });
    conversations.emplace_back(Conversation { "jeff2", 1618000006 });
    conversations.emplace_back(Conversation { "jer", 1618000008 });
    conversations.emplace_back(Conversation { "3344ff", 1618000010 });

    // this will represent the ConversationAdapter, just omitting the wrapper for simplicity
    auto conversationModel = new ConversationModel(conversations);
    QML_REGISTERSINGLETONTYPE_POBJECT(NS_MODELS, conversationModel, "ConversationModel")

    auto smartListModel = new SmartListModel(conversationModel);
    QML_REGISTERSINGLETONTYPE_POBJECT(NS_MODELS, smartListModel, "SmartListModel") // for Role enum
    auto smartListProxyModel = new SmartListProxyModel(smartListModel);
    QML_REGISTERSINGLETONTYPE_POBJECT(NS_MODELS, smartListProxyModel, "SmartListProxyModel")

    engine.load(url);

    return app.exec();
}
