import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.3
import QtQml.Models 2.3
import QtQuick.Window 2.3

import net.smartlist.Models 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 640
    height: 480
    minimumWidth: 640
    minimumHeight: 480

    Item {
        id: windowContainer
        anchors.fill: parent
    }

    property bool isFullScreen: false
    visibility: isFullScreen ? Window.FullScreen : Window.Windowed
    function toggleFullScreen() {
        isFullScreen = !isFullScreen
        itemView.parent = isFullScreen ?
                    windowContainer :
                    mainViewContainer
    }

    title: qsTr("Smartlist")

    SplitView {
        z: -1
        anchors.fill: parent
        orientation: Qt.Horizontal
        handle: Rectangle {
            implicitWidth: 4
            SplitView.fillHeight: true
        }

        Rectangle {
            implicitWidth: 200
            SplitView.minimumWidth: 150
            color: "lightblue"

            ColumnLayout {
                anchors.fill: parent
                spacing: 0
                Rectangle {
                    Layout.fillWidth: true
                    Layout.preferredHeight: 40
                    color: "lightgreen"
                    TextEdit {
                        id: addTextEdit
                        anchors.fill: parent
                        textMargin: 10
                        font.pointSize: 14
                    }
                    Button {
                        anchors.right: parent.right
                        width: 40
                        text: "+"
                        onClicked: ConversationModel.addConversation(addTextEdit.text)
                    }
                }
                Rectangle {
                    Layout.fillWidth: true
                    Layout.preferredHeight: 40
                    color: "pink"
                    TextEdit {
                        anchors.fill: parent
                        textMargin: 10
                        font.pointSize: 14
                        onTextChanged: SmartListProxyModel.setFilter(text)
                    }
                }
                ListView {
                    id: smartListView
                    focus: true
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    keyNavigationEnabled: false
                    model: SmartListProxyModel
                    currentIndex: SmartListProxyModel.currentFilteredRow
                    clip: true

                    highlight: Rectangle {
                        width: ListView.view ? ListView.view.width : 0
                        color: "lightsteelblue"
                    }
                    highlightMoveDuration: 0

                    delegate: ItemDelegate {
                        id: itemDelegate
                        text: Id + " " + TimeStamp
                        width: ListView.view.width
                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                SmartListProxyModel.select(index)
                                smartListView.focus = true
                            }
                            onDoubleClicked: {
                                ConversationModel.updateConversation(Id)
                            }
                        }
                        Button {
                            anchors.right: parent.right
                            width: 40
                            text: "x"
                            onClicked: ConversationModel.removeConversation(Id)
                        }
                    }

                    add: Transition {
                        NumberAnimation { property: "opacity"; from: 0; to: 1.0; duration: 250 }
                        NumberAnimation { property: "scale"; from: 0; to: 1.0; duration: 250 }
                    }

                    displaced: Transition {
                        NumberAnimation { properties: "x,y"; duration: 250; easing.type: Easing.OutBounce }
                    }

                    move: Transition {
                        NumberAnimation { properties: "x,y"; duration: 250; easing.type: Easing.OutBounce }
                    }
                }
            }
        }
        Item {
            id: mainViewContainer
            SplitView.minimumWidth: 150
            SplitView.fillWidth: true

            Rectangle {
                id: itemView

                anchors.fill: parent
                color: "lightgray"

                MouseArea {
                    anchors.fill: parent
                    onDoubleClicked: root.toggleFullScreen()
                }

                Label {
                    id: itemContentLabel
                    font.pointSize: 24

                    Connections {
                        target: SmartListProxyModel
                        function onValidSelectionChanged() {
                            var row = SmartListProxyModel.currentFilteredRow
                            var id = SmartListProxyModel.dataForRow(row, SmartListModel.Id)
                            itemContentLabel.text = id !== undefined ? id : ""
                        }
                    }

                    anchors.centerIn: parent
                }
                Button {
                    anchors.top: itemContentLabel.bottom
                    anchors.horizontalCenter: itemContentLabel.horizontalCenter
                    width: 100
                    text: "update"
                    onClicked: {
                        ConversationModel.updateConversation(itemContentLabel.text)
                    }
                }
            }
        }
    }
}
