#include "smartlistmodel.h"

SmartListModel::SmartListModel(ConversationModel* model, QObject* parent)
    : QAbstractListModel(parent)
    , model_(model)
{
    connect(
        model_, &ConversationModel::beginInsertRows, this,
        [this](int position, int rows) {
            beginInsertRows(QModelIndex(), position, position + (rows - 1));
        },
        Qt::DirectConnection);
    connect(model_, &ConversationModel::endInsertRows,
        this, &SmartListModel::endInsertRows, Qt::DirectConnection);

    connect(
        model_, &ConversationModel::beginRemoveRows, this,
        [this](int position, int rows) {
            beginRemoveRows(QModelIndex(), position, position + (rows - 1));
        },
        Qt::DirectConnection);
    connect(model_, &ConversationModel::endRemoveRows,
        this, &SmartListModel::endRemoveRows, Qt::DirectConnection);

    connect(model_, &ConversationModel::dataChanged, this,
        [this](int position) {
            const auto index = createIndex(position, 0);
            Q_EMIT SmartListModel::dataChanged(index, index);
        });
}

int SmartListModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    const ConversationQueue& data = model_->getUnfilteredData();
    return static_cast<int>(data.size());
}

QHash<int, QByteArray> SmartListModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Id] = "Id";
    roles[TimeStamp] = "TimeStamp";
    return roles;
}

QVariant SmartListModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid() || index.row() > rowCount()) {
        return {};
    }

    const ConversationQueue& data = model_->getUnfilteredData();
    auto& item = data.at(static_cast<size_t>(index.row()));
    switch (role) {
    case Role::Id:
        return QVariant(item.id);
    case Role::TimeStamp:
        return QVariant(item.timestamp);
    }

    return {};
}

SmartListProxyModel::SmartListProxyModel(QAbstractItemModel* parent)
    : QSortFilterProxyModel(parent)
    , selectedSourceIndex_(QModelIndex())
{
    setSourceModel(parent);
    setSortRole(SmartListModel::Role::TimeStamp);
    sort(0, Qt::DescendingOrder);
    connect(sourceModel(), &QAbstractListModel::dataChanged,
        this, &SmartListProxyModel::updateSelection);
    connect(sourceModel(), &QAbstractListModel::rowsInserted,
        this, &SmartListProxyModel::updateSelection);
    connect(sourceModel(), &QAbstractListModel::rowsRemoved,
        this, &SmartListProxyModel::updateSelection);
}

void SmartListProxyModel::setFilter(const QString& filterString)
{
    setFilterRegExp(filterString);
    updateSelection();
}

void SmartListProxyModel::select(const QModelIndex& index)
{
    selectedSourceIndex_ = mapToSource(index);
    updateSelection();
}

void SmartListProxyModel::select(int row)
{
    select(index(row, 0));
}

int SmartListProxyModel::currentFilteredRow()
{
    return currentFilteredRow_;
}

QVariant SmartListProxyModel::dataForRow(int row, int role) const
{
    return data(index(row, 0), role);
}

void SmartListProxyModel::setCurrentFilteredRow(int currentFilteredRow)
{
    if (currentFilteredRow_ == currentFilteredRow)
        return;
    currentFilteredRow_ = currentFilteredRow;
    Q_EMIT currentFilteredRowChanged(currentFilteredRow_);
}

void SmartListProxyModel::updateSelection()
{
    auto filteredIndex = mapFromSource(selectedSourceIndex_);

    // if the source model is empty, invalidate the selection
    if (sourceModel()->rowCount() == 0) {
        setCurrentFilteredRow(-1);
        Q_EMIT validSelectionChanged();
        return;
    }

    // if the source and filtered index is no longer valid
    // this would indicate that a mutation has occured,
    // thus any arbritrary ux decision is okay here
    if (!selectedSourceIndex_.isValid()) {
        auto row = qMax(--currentFilteredRow_, 0);
        selectedSourceIndex_ = mapToSource(index(row, 0));
        filteredIndex = mapFromSource(selectedSourceIndex_);
        currentFilteredRow_ = filteredIndex.row();
        Q_EMIT currentFilteredRowChanged(currentFilteredRow_);
        Q_EMIT validSelectionChanged();
        return;
    }

    // update the row for ListView observers
    setCurrentFilteredRow(filteredIndex.row());

    // finally, if the filter index is invalid, then we have
    // probably just filtered out the selected item and don't
    // want to force reselection of other ui components, as the
    // source index is still valid
    if (filteredIndex.isValid())
        Q_EMIT validSelectionChanged();
}

bool SmartListProxyModel::lessThan(const QModelIndex& left, const QModelIndex& right) const
{
    QVariant leftData = sourceModel()->data(left, SmartListModel::Role::TimeStamp);
    QVariant rightData = sourceModel()->data(right, SmartListModel::Role::TimeStamp);

    return leftData.toUInt() < rightData.toUInt();
}

bool SmartListProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const
{
    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    return (index.data(SmartListModel::Role::Id).toString().contains(filterRegExp()));
}
