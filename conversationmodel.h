#pragma once

#include <QDebug>
#include <QObject>
#include <QString>

#include <deque>

//using TimeType = QDateTime;
//#include <QDateTime>
//#define tNow QDateTime::currentDateTime()

using TimeType = time_t;
#include <chrono>
using namespace std::chrono;
#define tNow system_clock::to_time_t(system_clock::now())

struct Conversation {
    QString id;
    TimeType timestamp;
    Conversation(const QString& _id,
        TimeType _timestamp = tNow)
        : id(_id)
        , timestamp(_timestamp)
    {
    }
};
using ConversationQueue = std::deque<Conversation>;

class ConversationModel final : public QObject {
    Q_OBJECT
public:
    ConversationModel(const ConversationQueue& data, QObject* parent = nullptr)
        : QObject(parent)
        , data_(data)
    {
    }

    const ConversationQueue& getUnfilteredData() const { return data_; }

    Q_INVOKABLE void updateConversation(const QString& id)
    {
        if (id.isEmpty())
            return;
        auto it = std::find_if(data_.begin(), data_.end(),
            [&id](Conversation& c) {
                return c.id == id;
            });
        if (it != data_.end()) {
            auto row = std::distance(data_.begin(), it);
            it->timestamp = tNow;
            Q_EMIT dataChanged(static_cast<int>(row));
        }
    }

    Q_INVOKABLE void addConversation(const QString& id)
    {
        if (id.isEmpty())
            return;
        Q_EMIT beginInsertRows(static_cast<int>(data_.size()));
        data_.emplace_back(Conversation { id });
        Q_EMIT endInsertRows();
    }

    Q_INVOKABLE void removeConversation(const QString& id)
    {
        if (id.isEmpty())
            return;
        auto it = std::find_if(data_.cbegin(), data_.cend(),
            [&id](const Conversation& c) {
                return c.id == id;
            });
        if (it != data_.cend()) {
            auto row = std::distance(data_.cbegin(), it);
            Q_EMIT beginRemoveRows(static_cast<int>(row));
            data_.erase(it);
            Q_EMIT endRemoveRows();
        }
    }

Q_SIGNALS:
    void beginInsertRows(int position, int rows = 1);
    void endInsertRows();
    void beginRemoveRows(int position, int rows = 1);
    void endRemoveRows();
    void dataChanged(int position);

private:
    ConversationQueue data_;
};
