#pragma once

#include "conversationmodel.h"

#include <QAbstractListModel>
#include <QDebug>
#include <QObject>
#include <QSortFilterProxyModel>
#include <QString>

class SmartListModel : public QAbstractListModel {
    Q_OBJECT
public:
    explicit SmartListModel(ConversationModel* model, QObject* parent = nullptr);

    enum Role {
        Id = Qt::UserRole + 1,
        TimeStamp
    };
    Q_ENUM(Role)

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

private:
    ConversationModel* model_;
};

class SmartListProxyModel final : public QSortFilterProxyModel {
    Q_OBJECT
    Q_PROPERTY(int currentFilteredRow
            READ currentFilteredRow WRITE setCurrentFilteredRow NOTIFY currentFilteredRowChanged)

public:
    using FilterPredicate = std::function<bool(const QModelIndex&, const QRegExp&)>;

    explicit SmartListProxyModel(QAbstractItemModel* parent);

    virtual bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override;
    bool lessThan(const QModelIndex& left, const QModelIndex& right) const override;

    Q_INVOKABLE void setFilter(const QString& filterString);
    Q_INVOKABLE void select(const QModelIndex& index);
    Q_INVOKABLE void select(int row);
    Q_INVOKABLE int currentFilteredRow();
    Q_INVOKABLE QVariant dataForRow(int row, int role = Qt::DisplayRole) const;

public Q_SLOTS:
    void setCurrentFilteredRow(int currentFilteredRow);

private Q_SLOTS:
    void updateSelection();

Q_SIGNALS:
    void currentFilteredRowChanged(int currentFilteredRow);
    void validSelectionChanged();

private:
    QPersistentModelIndex selectedSourceIndex_;
    int currentFilteredRow_ { -1 };
};
